$env:GIT_REDIRECT_STDERR = '2>&1'



git init


git config --global --unset credential.helper
git config --global --unset user.name
git config --global --unset user.email


if (Test-Path "$HOME\.git-credentials") {
  Remove-Item "$HOME\.git-credentials"
}



###############################################



git config --global credential.helper store
Add-Content "$HOME\.git-credentials" "https://${env:APPVEYOR_REPO_COMMIT_AUTHOR}:${env:LOGIN}@github.com`n"


git config --global user.name "${env:APPVEYOR_REPO_COMMIT_AUTHOR}"
git config --global user.email "${env:APPVEYOR_REPO_COMMIT_AUTHOR_EMAIL}"


git config --global advice.detachedHead false
git config --global core.autocrlf false
