$env:COMPILER_VERSION = ${env:APPVEYOR_BUILD_WORKER_IMAGE}



#####################################



if ($env:SOLUTION -Like "*.sln" -or
    $env:SOLUTION -Like "*.vcxproj") {
  $env:BUILD = "msbuild `"${env:SOLUTION}`" /p:Configuration=`"${env:CONFIGURATION}`" /p:Platform=`"${env:PLATFORM}`" /m"
}


else {
  $env:BUILD = "make -f `"${env:SOLUTION}`" platform=`"${env:PLATFORM}`""
}



#####################################



$env:PATH = "C:\cygwin64\bin;" + $env:PATH
