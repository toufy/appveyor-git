image: ubuntu


shallow_clone: true


environment:
  script: .




install:

  - ps: |
      if (Test-Path "makefile_script.txt" -PathType Leaf) {
        ${env:script} = (Get-Content "makefile_script.txt" -raw)
      }




before_build:

  - sh: |
      cd .
      export DIR=$PWD


      export PATH="$DIR/tools/devkitpro/devkitPPC/powerpc-eabi:$PATH"
      export DEVKITPPC="$DIR/tools/devkitpro/devkitPPC"
      export DEVKITPRO="$DIR/tools/devkitpro"

      git clone https://github.com/libretro/RetroArch Retroarch

      wget "https://github.com/libretro/libretro-toolchains/blob/master/wiiu.tar.xz?raw=true" -O "wiiu.tar.xz"
      mkdir tools
      tar xf wiiu.tar.xz -C tools


      cd $DIR




build_script:
  - sh: |
      gcc -v

      cd $script
      make -f Makefile platform=wii




artifacts:
  - path: '**\pkg\**\*.dol'
