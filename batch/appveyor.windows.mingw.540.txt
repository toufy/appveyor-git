shallow_clone: true



environment:

  matrix:
  - Build: mingw32
    Arch: i686
    Port: win32
    Tool: mingw32
    MINGW_eh: dwarf

  - Build: mingw64
    Arch: x86_64
    Port: win64
    Tool: mingw64
    MINGW_eh: seh

  MINGW_version: 5.4.0

  # MINGW_api: win32
  MINGW_api: posix



  script: .




before_build:

  - cmd: |
      appveyor-retry appveyor DownloadFile "https://downloads.sourceforge.net/mingw-w64/%arch%-%MINGW_version%-release-%MINGW_api%-%MINGW_eh%-rt_v5-rev0.7z" -FileName "C:\mingw.7z"
      7z x c:\mingw.7z -oc:\


      set Path=c:\%Build%\bin;%Path%
      set Path=c:\%Build%\%arch%-w64-%Tool%\include;%Path%
      set Path=c:\%Build%\%arch%-w64-%Tool%\lib;%Path%
      set Path=c:\%Build%\lib\gcc\%arch%-w64-%Tool%\%MINGW_version%;%Path%
      set Path=c:\%Build%\libexec\gcc\%arch%-w64-%Tool%\%MINGW_version%;%Path%


      set CC=gcc
      set CXX=g++




build_script:

  - cmd: |
      g++ -v

      cd %script%
      mingw32-make -f Makefile platform=%Platform%




artifacts:
  - path: '**\*libretro.dll'
