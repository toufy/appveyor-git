image: ubuntu


shallow_clone: true



environment:
  script: .




install:

  - ps: |
      if (Test-Path "makefile_script.txt" -PathType Leaf) {
        ${env:script} = (Get-Content "makefile_script.txt" -raw)
      }




before_build:

  - sh: |
      cd .
      export DIR=$PWD


      git clone https://github.com/libretro/RetroArch Retroarch

      git clone https://github.com/emscripten-core/emsdk emsdk
      cd emsdk
      ./emsdk install latest
      ./emsdk activate latest
      source ./emsdk_env.sh


      cd $DIR




build_script:

  - sh: |
      cd $script

      emmake make -f Makefile platform=emscripten
