$env:GIT_REDIRECT_STDERR = '2>&1'



git init


git config --global credential.helper store
Add-Content "$HOME\.git-credentials" "https://${env:APPVEYOR_REPO_COMMIT_AUTHOR}:${env:LOGIN}@github.com`n"

git config --global user.name "${env:APPVEYOR_REPO_COMMIT_AUTHOR}"
git config --global user.email "${env:APPVEYOR_REPO_COMMIT_AUTHOR_EMAIL}"

git config --global advice.detachedHead false
git config --global core.autocrlf false



#################################################



$DEPTH = 500

$env:BATCH_BRANCH = "@@___make_batch___##"



git remote add origin "https://github.com/${env:APPVEYOR_REPO_NAME}.git"

git fetch origin --depth=$DEPTH --no-tags



#################################################



git push origin :${env:BATCH_BRANCH}

git checkout -b ${env:BATCH_BRANCH} origin/${env:APPVEYOR_REPO_BRANCH}



Set-Content -Path "makefile_script.txt" -Value ${env:Makefile_Script}



#################################################



echo "`n"



#################################################



function global:add_build($platform) {
  del appveyor-git.yml
  git add . --force

  # git push origin HEAD:${env:BATCH_BRANCH}



  # ============================================== #



  Copy-Item -Path c:\appveyor-git-master\batch\${platform} -Destination c:\projects\${env:APPVEYOR_PROJECT_SLUG}\appveyor-git.yml


  git add . --force
  git commit -m "${platform}"

  git push origin HEAD:${env:BATCH_BRANCH}
}
