image: Visual Studio 2017


shallow_clone: true



environment:
  Android_NDK: 'C:\ProgramData\Microsoft\AndroidNDK64\android-ndk-r17'


  script: .




install:

  - ps: |
      if (Test-Path "makefile_script.txt" -PathType Leaf) {
        ${env:script} = (Get-Content "makefile_script.txt" -raw)
      }




build_script:

  - cmd: |
      set Path=%Android_NDK%;%Path%
      cd %script%\jni && ndk-build


      ren %script%\libs\arm64-v8a\libretro.so %APPVEYOR_PROJECT_NAME%_libretro_android.so
      ren %script%\libs\armeabi-v7a\libretro.so %APPVEYOR_PROJECT_NAME%_libretro_android.so
      ren %script%\libs\x86\libretro.so %APPVEYOR_PROJECT_NAME%_libretro_android.so
      ren %script%\libs\x86_64\libretro.so %APPVEYOR_PROJECT_NAME%_libretro_android.so




artifacts:
  - path: '**\libs\**\*.so'
