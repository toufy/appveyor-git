image: Visual Studio 2017


shallow_clone: true




environment:
  matrix:
    - Platform: windows_msvc2017_uwp_x86
    - Platform: windows_msvc2017_uwp_x64
    - Platform: windows_msvc2017_uwp_arm


  script: .




install:

  - ps: |
      if (Test-Path "makefile_script.txt" -PathType Leaf) {
        ${env:script} = (Get-Content "makefile_script.txt" -raw)
      }




build_script:

  - cmd: |
      set Path=C:\msys64\usr\bin;%Path%

      cd %script%
      make -f Makefile platform=%Platform%




artifacts:
  - path: '**\*libretro.dll'
