$url = "https://github.com/${env:USER}?tab=repositories"



echo "`n`nGithub: ${env:USER}`n`n"



##################################



while(1) {
  curl -o "C:\list.txt" $url

  if ($? -Eq $False) {
    break
  }




  foreach ($repo in Select-String -Path "C:\list.txt" -Pattern "<a href=`"(.+)`" itemprop=`"name codeRepository`"" ) {
    $repo = ($repo -Split "<a href=`"/${env:USER}/(.+)`" itemprop=`"name codeRepository`"")[1]


    curl -o "C:\repo.txt" "https://github.com/${env:USER}/$repo"

    if ((Get-Content "C:\repo.txt" -Raw) -Match "(\d+) commit(s)? behind" -Eq $True) {
      echo "$repo  --  $($matches[0])"
    }


    Start-Sleep -m 4
  }




  if ((Get-Content "C:\list.txt" -Raw) -Match "https://github.com/${env:USER}\?after=(.+)`">Next" -Eq $False) {
    break
  }

  $url = "https://github.com/${env:USER}?after=$($matches[1])"
}



##################################



echo "`n"
