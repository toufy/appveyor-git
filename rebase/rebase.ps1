if (${env:SQUASH} -Eq "yes") {
  ${env:SQUASH} = 0
}

else {
  ${env:SQUASH} = -1
}



#################################################



git remote add origin "https://github.com/${env:APPVEYOR_REPO_NAME}.git"

git fetch origin --no-tags



if ($env:ERASE -Ne 0) {
  git checkout origin/${env:APPVEYOR_REPO_BRANCH}

  git reset --hard HEAD~${env:ERASE}

  git push -f origin HEAD:${env:APPVEYOR_REPO_BRANCH}
}



#################################################



curl -o "C:\repo.txt" "https://github.com/${env:APPVEYOR_REPO_NAME}"


$UPSTREAM = (((Get-Content "C:\repo.txt" -Raw) -Split "forked from")[1] -Split "`">(.+)</a>")[1]

$DEFAULT = ((Get-Content "C:\repo.txt" -Raw) -Split "<span class=`"css-truncate-target`" data-menu-button>(.+)</span>")[1]



#################################################



git remote add upstream "https://github.com/${UPSTREAM}.git"

git fetch upstream master:upstream --no-tags
git fetch upstream ${DEFAULT}:upstream --no-tags



$branches = git branch -r | Select-String -NotMatch "upstream"


foreach ($branch in $branches) {
  $branch = (($branch -Replace '(^\s+|\s+$)','') -Split '/')[1]

  echo "`n${branch}"



  git checkout origin/${branch}


  iex ((New-Object System.Net.WebClient).DownloadString("${env:SCRIPT}/squash/squash.ps1"))



  ### ================================================ ###



  if ($branch -Eq "master") {
    git rebase upstream/master
  }

  else {
    git rebase upstream/${DEFAULT}
  }




  if ($? -Eq $False) {
    git diff --diff-filter=U


    if (${env:FORCE} -Ne "yes") {
      throw "${branch}: rebase failure"
    }


    git add .
    git rebase --continue
  }



  ### ================================================ ###



  git push -f origin HEAD:${branch}


  if ($? -Eq $False) {
    throw "${branch}: rebase failure"
  }
}



#################################################



echo "`n"
