if ($env:CLEAN -Eq "yes") {
  cd $env:APPVEYOR_BUILD_FOLDER

  move $env:ARCHIVE ..\
  move .git* ..\



  cd ..
  Remove-Item -Recurse -Force $env:APPVEYOR_BUILD_FOLDER
  New-Item $env:APPVEYOR_BUILD_FOLDER -itemtype directory



  move $env:ARCHIVE $env:APPVEYOR_BUILD_FOLDER
  move .git* $env:APPVEYOR_BUILD_FOLDER

  cd $env:APPVEYOR_BUILD_FOLDER
}



##########################################



7z x $env:ARCHIVE -aoa
del $env:ARCHIVE

git add .



##########################################



$title = git log -1 --pretty=format:'%s'


git reset --soft HEAD~1
git commit --amend -m "$title" --date "$(date)"
