if (${env:SQUASH} -Eq "yes") {
  ${env:SQUASH} = 0
}

else {
  ${env:SQUASH} = -1
}



#################################################



git remote add origin "https://github.com/${env:APPVEYOR_REPO_NAME}.git"

git fetch origin --no-tags



if ($env:ERASE -Ne 0) {
  git checkout origin/${env:APPVEYOR_REPO_BRANCH}

  git reset --hard HEAD~${env:ERASE}

  git push -f origin HEAD:${env:APPVEYOR_REPO_BRANCH}
}



#################################################



curl -o "C:\repo.txt" "https://github.com/${env:APPVEYOR_REPO_NAME}"



$upstream = (((Get-Content "C:\repo.txt" -Raw) -Split "forked from")[1] -Split '/')[1]

if ($upstream -Eq $Null) {
  $upstream = $env:APPVEYOR_REPO_COMMIT_AUTHOR
}


$default = (Get-Content "C:\repo.txt" -raw) -Match "<span class=`"css-truncate-target`" data-menu-button>(.+)</span>"
$default = $matches[1]



#################################################



git remote add upstream "https://github.com/${upstream}/${env:APPVEYOR_PROJECT_NAME}.git"

git fetch upstream master:upstream --no-tags
git fetch upstream ${default}:upstream --no-tags



$branches = git branch -r | Select-String -NotMatch "upstream"


foreach ($branch in $branches) {
  $branch = (($branch -Replace '(^\s+|\s+$)','') -Split '/')[1]

  echo "`n${branch}"



  git checkout origin/${branch}


  iex ((New-Object System.Net.WebClient).DownloadString("https://github.com/${env:SCRIPT}/appveyor-git/raw/master/squash/squash.ps1"))



  ### ================================================ ###



  if ((git log -1 --pretty=format:'%an') -Match ${env:APPVEYOR_REPO_COMMIT_AUTHOR} -Eq $False) {
    continue
  }



  git commit --amend --author="${env:APPVEYOR_REPO_COMMIT_AUTHOR} <${env:APPVEYOR_REPO_COMMIT_AUTHOR_EMAIL}>" --no-edit
  git push -f origin HEAD:${branch}
}



#################################################



echo "`n"
